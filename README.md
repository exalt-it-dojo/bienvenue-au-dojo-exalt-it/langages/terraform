# Doctrine Terraform

# 🎓 Junior

> Connaître le workflow usuel de Terraform & les principales commandes de CLI

## Hashicorp Configuration Language (HCL)

Un **langage de configuration** développé par Hashicorp.
Langage de configuration = comme YAML, JSON ...

<details>
<summary>Quel(s) avantage(s) d'utiliser HCL plutôt qu'un autre langage de configuration dans Terraform ?</summary>
<br>

L'objectif d'un langage de configuration = être aussi human-readible que possible, en proposant une manière élégante d'écrire la configuration. Dans le cas du provisionnement d'infrastructure, le HCL se démarque notamment pour sa manière de gérer les dépendances entre composants, et la possibilité de faire de l'interpolation de chaine de caractères, utiliser des structures de données complexes, faire appel à des fonctions ... ce qui lui donne une grande expressivité par rapport à du YAML ou du JSON.


</details>
<br>


HCL adopte le paradigme **déclaratif**.


<details>
<summary>Dans le cadre du provisionnement d'infrastructure, quelle est la différence entre une approche déclarative & une approche impérative ?</summary>
<br>

* Déclaratif: je dis ce que je veux, pas comment le faire. 
  
  -> j'écris l'état souhaité de l'infrastructure, et Terraform se charge de le mettre en oeuvre

* Impératif: je donne des instructions de manière séquentielle.
  
  -> J'utilise la CLI AWS pour créer mon bucket S3, lui ajouter une Policy, uploader un fichier dedans ...  


</details>
<br>


Les fichiers Terraform sont donc des **fichiers de configuration**, qui prennent par convention l'extension _.tf_ .
Par défaut, Terraform lit tous les fichiers _.tf_ du dossier courant **sans considération d'ordre**.

L'atome de la syntaxe HCL s'appelle **block**.

<details>
<summary> ⚠️ Quels types de blocks je trouve dans mes fichiers HCL ? ⚠️</summary>
<br>

* **Block Provider**: spécifie la configuration d'un Provider: plugin permettant à Terraform d'intéragir avec des APIs externes, notamment les APIs des cloud providers publics (AWS, Azure, GCP, ... _(mais pas que)_)

![provider](./medias/provider.png)

* **Block Resource**: la raison pour laquelle on utilise Terraform. Spécifie une ressource à faire provisionner via un Provider, qui sera managée par Terraform.

![resource](./medias/resource.png)

* **Block Data**: utilisé pour récupérer des informations de sources de données externes (ex: une API distante, un fichier local, une base de données, ...)

![data](./medias/data.png)


* **Block Variable** : utilisé pour définir les **valeurs d'entrée** de la configuration 

![variable](./medias/variable.png)

* **Block Output**: utilisé pour définir les valeurs de sorties de notre configuration, (par défaut, les valeurs seront loggées dans le flux STDOUT .json après exécution de Terraform)

![output](./medias/output.png)


* **Block Configuration** : blocks complémentaires permettant de spécifier les version de Terraform & plugins à utiliser (détaillé plus tard dans ce document).

</details>
<br>

## Workflow Terraform: Write, Plan, Apply

![Alt text](./medias/workflow.png)


### Questions

<details>
<summary>Qu'est ce que le Resource Graph terraform ? à quel moment du workflow est-il généré et pour quelle finalité ?</summary>
<br>

C'est une représentation de l'ensemble des ressources déclarées dans la configuration terraform. Il est généré lors de la phase de Plan (commande `terraform plan` et permet de voir les dépendances entre les différentes ressources afin de les créer / détruire dans le bon ordre. Passé la phase de plan, il n'est plus utile et est donc détruit.

</details>
<br>

<details>
<summary>⚠️ Concrètement, qu'est ce qu'un plan terraform ? ⚠️</summary>
<br>

C'est un fichier binaire généré par la commande `terraform plan` qui contient une représentation de toutes les opérations à faire (création, suppression, modification) pour faire évoluer l'infrastructure existante en infrastructure cible.

</summary>
</details>
<br>

<details>
<summary>A quel moment Terraform communique-t-il avec les APIs des cloud providers externes ?</summary>
<br>

* Phase Apply: pour provisionner les blocks 'resources' (ex: aws_instance, aws_vpc, ...)
* Phase Plan:
  * pour scanner l'infrastructure existante et détecter des potentiels changement ayant eu lieu sur ces dernières en faisant une comparaison avec le fichier state.
  * pour récupérer les valeurs des blocks 'data' (ex: récupération des AZ disponibles dans le région aws "eu-west-1")
  

</summary>
</details>
<br>


<details>
<summary>A quel moment le fichier state est-il suiceptible de changer ?</summary>
<br>

* Phase Apply: après le provisionnement des infrastructures déclenché par la commande `terraform apply`. Les infrastructures ayant pu être provisionnées seront renseignées dans le fichier state, les autres non.
* Phase Write: via la commande `terraform import`, on peut ajouter au state des ressources ayant été provisionnées manuellement, ou depuis un autre outil de provisionnement.
  

</summary>
</details>
<br>


<details>
<summary>Je souhaite déployer des resources sur AWS en utilisant mes credentials AWS (~/.aws/credentials). A quel moment je configure le plugin AWS necéssaire, et à quel moment les erreurs d'authentification ou de droits sont-elles suiceptibles d'apparaitre ?</summary>
<br>

Dans la phase Write, je créé un **block de configuration global terraform** ainsi qu'un **block provider**. 

La commande `terraform init` me permet d'installer les fichiers binaires exécutable necéssaire en local.

L'attribut `shared_credential_file` du provider AWS me permet de renseigner le fichier _~/.aws/credentials_. Si les credentials ne sont pas corrects, la commande `terraform init` échouera.

En revanche, les erreurs liées à l'API provider passée la phase d'authentification ont lieu dans la phase `terraform apply`. Par exemple, si le user IAM associé aux credentials AWS n'a pas les droits suffisants pour créer les ressources demandées.

![conf](./medias/configuration.png)

</summary>
</details>
<br>

![cycle](./medias/cycle.png)

TODO: question sur les avantages d'utiliser Terraform

<br>


## State


![state](./medias/state.png)

<details>
<summary>Quelle est la responsabilité du state ?</summary>
<br>

* Stocker l'état des ressources: attributs & métadonnées settées côté provider.
* Contrôler la modification des ressources: quand mon fichier de conf est modifié, je garde traçe des ressources côté provider qui doivent être modifiées / détruites et recrées.
* Permettre d'altérer l'infra, plutôt que de la détruire et recréer à chaque modification qu'on souhaite apporter.
</summary>
<br>

</details>

<br>

<details>
<summary>Que se passe t'il au niveau du state lorsque la ressource est modifiée côté provider manuellement, ou en utilisant un autre outil de gestion de provisionnement ?</summary>
<br>

Le state n'est pas alteré.

> Le prochain run de `terraform apply` ou `terraform apply` détectera le changement et proposera de mettre l'infrastructure en conformité vis à vis des fichiers de conf (qui est donc alligné avec le state, si jamais la dernière exécution de `terraform apply` avait réussi).

La modification des infrastructures managées par terraform sans passer par la modification des fichiers de configuration s'appelle **State Drifting** et est généralement considerée comme  bad practice, du moins non pérenne.

Néanmoins, si on veut que le state drifting soit pris en compte dans le state, on peut utiliser la commande `terraform apply -refresh-only`. Ici encore, les fichiers de conf .tf ne sont pas altérés, et le prochain `terraform apply` aura pour effet de mettre les ressources du provider ET le fichier state en conformité vis-à-vis des fichiers de conf.


**EN BREF: la modification du fichier state en passant par la CLI est généralement CONTRE-INDIQUÉ**. La modification en direct dans le fichier state via un éditeur de texte est complètement proscrite.

</details>
<br>

<details>
<summary>Pourquoi HashiCorp nous donne accès à des commandes de CLI pour altérer notre state, alors qu'il est généralement déconseillé de le faire, et de préférer passer à 100% par la modification des fichiers de config ?</summary>
<br>

Parceque dans la vraie vie, on ne peut pas garantir que toute l'infra soit gerée à 100% par Terraform.

* présence d'autres outils de gestion d'infrastructure dans la stack
* architecture, composants trop complexes à migrer sur Terraform
* manque de connaissance / résistance au changement des équipes

Dans ces cas là, l'utilisation de la CLI permet de modifier le state Terraform de manière à ce qu'il réflète la réalité de l'infra effectivement provisionnée.

</details>
<br>
<br>

# 🧪 Confirmé

> Compréhension avancée de la gestion du backend terraform et des best practice en terme de collaboration & de sécurité.


## Backend

![backend](./medias/backend.png)

<details>
<summary>Quelle est la responsabilité du backend ?</summary>
<br>

Le backend est une abstraction à travers laquelle on lit et modifie le fichier state.

> Le backend par défaut est local: le fichier state produit par `terraform apply` est conservé dans le dossier courant. Cependant, cette option n'est pas adaptée à la collaboration (le fichier state ne devant pas être versionné dans Git). Elle n'est pas non plus adaptée à l'éxécution de Terraform dans des environnements temporaires (par exemple, un job de CI), car le fichier state serait donc perdu à la fin du job. Quand on utilise Terraform en situation réelle, on priviligera donc l'utilisation d'autres backend.


Le backend doit assurer:

* le **stockage du fichier state** (état de l'infrastructure)
* la possibilité de **collaborer** sur un projet avec 1 state pour X contributeurs
* l'intégrité du fichier state (gestion des conflits liés à des accès concurrents -> **state locking**)
* la **sécurité** du state: chiffrement des fichiers, masque des données sensibles contenues dans le state, ...


> Note: dans le cas du backend "Remote", également appelé "_enhanced backend_", [proposé par Terraform](https://app.terraform.io/public/signup/account?product_intent=terraform), le backend a d'autres fonctionnalités, comme la gestion des variables d'environnement, la supervision et l'exécution du workflow.
> **Dans tous les autres cas cependant, le backend se limite aux responsabilités listées ci-dessus**, et l'exécution se fait sur la machine de l'utilisateur.

  
</details>
<br>


<details>
<summary>Sur un backend S3, quels sont les composants qui permettent la sécurité & l'integrité de notre fichier state ?</summary>
<br>

![statelocking](./medias/s3_backend_state_locking.png)

* **bucket S3** -> conservation du fichier state.
* **chiffrement du bucket s3** -> sécurisation du state: 
* **version control S3** -> possibilité de switch le state sur des versions précédentes
* **State Locking** -> table DynamoDB conservant un fichier .md5 indiquant si le state est en cours de modification par un autre utilisateur ou non. Si le state est en cours de modification par un utilisateur, on refuse l'accès à tout autre utilisateur, qui attendent donc leur tour.

</details>

<br>

## Modules

Le module: l'unité logique permettant de **grouper un ensemble de ressources** pouvant être **réutilisées** ensemble de manière cohérente.


Un module encapsule des ressources avec des variables d'entrée et de sortie qui permettent respectivement de définir certaines propriétés des ressources
à provisionner dans le module, et de donner accès à certaines propriétés des ressources provisionnées par le module.

![module](./medias/module.png)

> Pour faire une analogie avec le _atomic design_, on pourrait dire:
> * Atome -> Block
> * Molécule -> Module
> * Organisme -> Module "Root"


<details>
<summary>Quelles sont les principales guidelines à suivre pour créer un module ?</summary>
<br>

- définir 1 fonctionnalité claire.
- pour cette fonctionnalité, viser à traiter 80% des cas. Les gens qui tombent dans les 20% restants n'ont qu'à fork le module et implémenter leur version.
- exposer toutes les variables d'entrée / sortie dont les utilisateurs sont susceptibles d'avoir besoin (et elles seules).
- isoler autant que possible des dépendances externes.
- toujours viser la réutilisabilité.

</details>
<br>

<details>
<summary>Comment sourcer un module terraform ?</summary>
<br>

1. Déterminer la source du module:
- module développé en local. Ex: `source = "./modules/my_vcp_module"`.
- module du [terraform registry](https://developer.hashicorp.com/terraform/language/modules/sources#terraform-registry). Ex: `source = "terraform-aws-modules/vpc/aws"`
- repository git. La source est interpretée comme un url de `git clone`, pouvant être réalisé en ssh ou https. Ex: `source = "git@github.com:hashicorp/example.git"`
- et bien d'autres !

> HashiCorp encourage à fond le partage & la réutilisation des modules, d'où l'importante flexibilité par rapport à leur modalité d'import. Dans la plupart des cas, un module existe pour traiter le besoin. Il est donc important de chercher dans le registry public, ou le registry privé de l'organisation, avant de réinventer la roue.

</details>
<br>

<br>

# 🧙‍♂️ Senior

> Maîtrise des différentes abstractions de Terraform et de son fonctionnement sous-jacent.


<details>
<summary>Si on prend un peu de recul, Terraform est composé de 2 blocs distincts. Que sont-ils et quelles sont leurs reponsabilités respectives ?</summary>
<br>

1. Terraform Core
> Binaire compilé statiquement en local (dossier .terraform/)

Responsable de:
* IaC: lecture des fichiers de configuration .tf
* Construction du Resource Graph
* Communication avec le backend
* Communication avec les plugins


2. Terraform Plugins
> Binaires invoqués par Core en RPC (remote procedure calls)

Responsables de:

* Résolution de la phase d'application: communication avec les APIs des providers d'infrastructure.
* définition des types de ressources à intégrer dans le Resource Graph lors de phase de plan.
</details>
<br>


> Dans tous les cas, ces binaires sont générés dans le dossier d'éxécution de Terraform par la commande `terraform init`.


<details>
<summary>Un provider n'est finalement qu'une interface vers un fournisseur de service. Si je veux créer un provider custom me permettant de créer de nouvelles pages sur un wiki Notion, comment m'y prendre ?</summary>

![notionlogo](./medias/notionlogo.webp)

1. Utiliser le SDK Terraform qui permet de développer des modules customisés en Go.

Archi Controller-Service-Repository:

2. Repository: Définir les ressources & sources de données de mon provider. Ex: "Page", "RichText", "Image"
3. Controller: exposer les ressources au travers d'un CRUD qui sera appelé lors du `terraform apply`.
4. Service: Créer un NotionService, appelé depuis le controller, qui intéragit avec l'API Notion en HTTP. Pour des raisons de sécurité, l'API Notion sera bien entendu accedée au travers d'une authentification, ou à minima une clé d'API.
5. Définir une variable d'entrée "NotionApiKey", qui sera utilisée en Header dans les requêtes HTTP de NotionService.
<br>



</details>
